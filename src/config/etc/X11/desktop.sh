#!/bin/sh
# This script is run by the S90UI script when 
# starting the UI.
# ----------------------------------------------

# PIDFILE holds the process id of the server
PIDFILE="/var/run/appmgr.pid"

# How do we start daemons
START_DAEMON="start-stop-daemon -S -b -q -m --pidfile ${PIDFILE} -x "

# How do we stop daemons
STOP_DAEMON="start-stop-daemon -K --pidfile ${PIDFILE} --retry 5"

case "$1" in
    start)
        # Matchbox destkop setup.
        # Copy matchbox session files to /root/.matchbox
        mkdir -p /root/.matchbox
        cp /etc/matchbox/* /root/.matchbox/

        # Test for appmgr.  Start it, if found.
        if [ -f /usr/bin/appmgr ]
        then
            ${START_DAEMON} /usr/bin/appmgr
            # /usr/bin/appmgr &
            # pid=$!
            # echo $pid > /var/run/appmgr.pid
        fi
        ;;

    stop)
        # Kill appmgr, if running.
        # if [ -f /var/run/appmgr.pid ]
        # then
            # kill `cat /var/run/appmgr.pid`
            # rm -f /var/run/appmgr.pid
        # fi

    	if [ -e ${PIDFILE} ]; then
        	echo "Stopping appmgr."
        	${STOP_DAEMON}
        	# shellcheck disable=SC2181
        	if [ $? -eq 0 ]; then
            	rm -f ${PIDFILE}
            	exit 0
        	else
            	echo "Failed to stop appmgr daemon.  Forcing removal of PID file."
            	rm -f ${PIDFILE}
            	exit 1
        	fi
    	else
        	echo "Appmgr daemon is not running."
        	exit 0
    	fi
        ;;

    *) ;;
esac
