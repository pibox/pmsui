#!/bin/sh
# mdev handler for USB Input Event devices.
# Basically, this means check for keyboards we need to support under X.
# This script can be called asynchronously, so each call must serialize
# it's handling through piboxd.
# ---------------------------------------------------------------------
# Piboxd listens on this port on localhost only.
PIBOX_PORT=13910
tmpfile="$(mktemp -u -t keyb-XXXXXX)" 

# Extract USB vendor and product IDs from the mdev environment
ids="$(echo ${DEVPATH} | cut -f10 -d"/")"
vendor="$(echo ${ids} | cut -f2 -d":")"
product="$(echo ${ids} | cut -f1 -d":")"

# Generate an MT_SYS/MA_KEYB message.
header="00000209"
printf "0: %s" $header | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r > "${tmpfile}"

# Generate the payload size
size=$(echo "${vendor}:${product}"|wc -c)
printf "8: %.8x" $size | sed -E 's/8: (..)(..)(..)(..)/8: \4\3\2\1/' | xxd -r >> "${tmpfile}"

# Write the USB IDs as the payload. This isn't strictly necessary, but it's for completeness sake.
echo -ne "${vendor}:${product}" >> "${tmpfile}"

# Send the message to piboxd to serialize requests.
nc -x localhost ${PIBOX_PORT} < "${tmpfile}" >/dev/null 2>&1 

# Clean up
rm -f "${tmpfile}"
